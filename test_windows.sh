#!/usr/bin/env bash
mingw32-make
[[ "$?" != "0" ]] && exit 1
for i in {1..50};
do
    number=$( printf "%X" ${i} )
    echo "Number = $number"
    ./prog_old_windows $number > expected.txt
    ./prog $number > actual.txt
    diff expected.txt actual.txt
done
number=''
for i in {1..50};
do
    number=$number'A'
    echo "Number = $number"
    ./prog_old_windows $number > expected.txt
    ./prog $number > actual.txt
    diff expected.txt actual.txt
done
echo $?
