# Clean Code Rewriting from Obfuscated code

This repository contains what was a game to refactor one of the winner of the
*24th International Obfuscated C Code Contest (2015)* code into cleanner code.

The code was copied from [the website directly](http://www.ioccc.org/2015/schweikhardt/prog.c),
and I'd like to thank *Jens Schweikhardt* for submitting it.

## Current state
This code was written in 3 session of 2 hours, during lunch times by the following people:
 - Jerome Mazeries;
 - Florent Imbro;
 - François Goasmat.

Each commit contains the date of the refactoring, so moving through the commits, you can see
how the programme evolved, from 0 knowledge of what the programme did at first, until now,
where its structure is a lot clearer, and, hopefully, its code as well.

## How to compile? (Windows)
Simply run `bash test_windows.sh`, and the programme will be compiled, and a serie of tests
will be ran against the origin version of the programme with different inputs to show that
the result of the refactor is identical to the original.

Note: it assumes you have installed `mingw32-make`, and a pre-compiled version of the original code is provided as `prog_old_windows.exe`. You may have to recompile this one from the original source, or from commit `9dfa45ed63ae`.

## Howe to compile? (Linux)
Run `bash test_linux.sh`.

Note: a pre-compiled version of the original code is provided as `prog_old_linux`, but you may have to recompile this one from the original source, or from commit `9dfa45ed63ae`.
