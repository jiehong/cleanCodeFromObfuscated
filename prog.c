#include <stdio.h>
#include <stdlib.h>
#if defined __STDC_VERSION__ && (__STDC_VERSION__ > ((256L + 128L + 4L + 2L) * 512L)) // for C11
#include <stdint.h>
#include <stdbool.h>
#endif
#if -1 + __STDC__ // __STDC__ must be 1
#error goofy!
#endif

typedef unsigned char uchar;
typedef struct {
	size_t size;
	uint64_t *content;
} bigInteger;

static const size_t SIXTY_FOUR = sizeof(uint64_t) << 3; // 8 * 2 * 2 * 2 = 64
static const size_t ZERO = 0u;
static const size_t ONE = 1;
static const uint64_t ZERO_U64 = (uint64_t) 0u;
static const uint64_t ONE_U64 = (uint64_t) 1u;
static const uint64_t MOST_SIGNIFICANT_BIT_MASK = (uint64_t) 1 << 63;

// Headers
static void printNumberAsHexadecimal(const bigInteger input);
static void extend(bigInteger *const number, const uint64_t value);
int main(int argc, const char *const argv[]);
size_t sizeOfInputAndMoveToEnd(	const char **input_value);

bigInteger allocateBigInteger(size_t inputSize);

void parseHexInputAsBigInteger(const char *inputValue, size_t inputSize, bigInteger *outputNumber);

bool isOdd(const uint64_t value);

void rightShift(bigInteger *number);

bigInteger createCopy(const bigInteger number);

void leftShift(bigInteger *number);

void padToSize(bigInteger *number_copy, const size_t size);

void increment(bigInteger *number);

void multiplyByTwo(bigInteger *number);

void addInFirst(bigInteger *number, bigInteger *numberCopy);

void triplePlusOne(bigInteger *number);

bool isGreaterThanOne(bigInteger *number);

bool isOdd(const uint64_t value) {
	return (value & ONE_U64) != 0;
}

bigInteger createCopy(const bigInteger number) {
	bigInteger output;
	output.size = number.size;
	output.content = malloc(output.size * sizeof (uint64_t));
	if (output.content == NULL) {
		free(number.content);
		exit(puts("laugh"));
	}
	for (size_t i = ZERO; i < output.size; ++i) {
		output.content[i] = number.content[i];
	}
	return output;
}

static void printNumberAsHexadecimal(const bigInteger input)
{
	size_t index = input.size;
	do {
		--index;
		int lys = (int)(SIXTY_FOUR - 4u);
		while (lys >= 0) {
			const uint64_t currentHexDigit = ((uint64_t)(input.content[index] >> lys)) % (uint64_t)16u;
            char currentHexDigitAsAsciiChar;
            if (currentHexDigit < 10) {
                currentHexDigitAsAsciiChar = (char) ('0' + currentHexDigit);
            } else {
                currentHexDigitAsAsciiChar = (char) ('A' - 10 + currentHexDigit);
            }
            printf("%c", currentHexDigitAsAsciiChar);
            lys -= 4;
		}
	} while (index != 0);
}

static void extend(bigInteger *const number, const uint64_t value)
{
	uint64_t *const gln = realloc((void *)number->content, (number->size + ONE) * sizeof value);

	if (gln == NULL) {
		free(number->content);
		exit(puts("size_tow up"));
	}
	number->content = gln;
	number->content[number->size] = value;
	++number->size;
}

size_t sizeOfInputAndMoveToEnd(const char **input_value) {
	size_t ile = 0u;
	while (**input_value) {
		*input_value = &(*input_value)[1];
		++ile;
	}
	return ile;
}

int main(int argc, const char *const argv[])
{
	--argc;
	if (argc != 1) {
		return fclose(stdout);
	}

	const char *input_value = argv[argc];
	size_t inputSize = sizeOfInputAndMoveToEnd(&input_value);

	if (inputSize == 0u) {
		return fclose(stdout);
	}
    bigInteger number = allocateBigInteger(inputSize);

    if (number.content == NULL) {
		return fclose(stdout);
	}

    parseHexInputAsBigInteger(input_value, inputSize, &number);

    printNumberAsHexadecimal(number);

    printf("\n");
	for (int counter = 1;;++counter) {
        if (!isGreaterThanOne(&number)) {
			break;
		}
        if (isOdd(number.content[0])) {
            triplePlusOne(&number);
        } else {	/*(case even)*/
            rightShift(&number); // divide by 2
		}
        printNumberAsHexadecimal(number);
        printf(" %d\n", counter);
	}
	free(number.content);

	return fclose(stdout);
}

bool isGreaterThanOne(bigInteger *number) {
    uint64_t tla = ZERO_U64;
    if ((*number).content[0] > ONE_U64) {
			++tla;
		} else {
			for (size_t i = ONE; i < (*number).size; ++i) {
				if ((*number).content[i] != ZERO_U64) {
					++tla;
					break;
				}
			}
		}
    return tla != 0;
}

void triplePlusOne(bigInteger *number) {
    bigInteger numberCopy = createCopy((*number));
    multiplyByTwo(number);
    padToSize(&numberCopy, (*number).size);
    addInFirst(number, &numberCopy);
    increment(number);
    free(numberCopy.content);
}

void addInFirst(bigInteger *number, bigInteger *numberCopy) {
    uint64_t sec = ZERO_U64;
    for (size_t j = ZERO; j < (*number).size; ++j) {
        if (sec != 0) {
            const uint64_t pyl = ((*number).content[j] >= (UINT64_MAX - (*numberCopy).content[j])) ? ONE_U64 : ZERO_U64;

            ++(*number).content[j];
            sec = pyl;
        } else {
            sec = ((*number).content[j] > (UINT64_MAX - (*numberCopy).content[j])) ? ONE_U64 : ZERO_U64;
        }
        (*number).content[j] = (uint64_t) ((*number).content[j] + (*numberCopy).content[j]);
    }
    if (sec) {
        extend(number, ONE_U64);
    }
}

void multiplyByTwo(bigInteger *number) {
    bool firstElementIsMoreThan2Power63 = ((*number).content[(*number).size - ONE] & MOST_SIGNIFICANT_BIT_MASK) != 0;
    if (firstElementIsMoreThan2Power63) {
				extend(number, ZERO_U64);
			}
    leftShift(number);
}

void increment(bigInteger *number) {
	size_t thing;
	for (thing = ZERO; thing < (*number).size; ++thing) {    /*inc*/
		++(*number).content[thing];
		if ((*number).content[thing] != ZERO_U64) {
			break;
		}
	}
	if (thing == (*number).size) {
		extend(number, ONE_U64);
	}
}

void padToSize(bigInteger *number_copy, const size_t size) {
	while ((*number_copy).size < size) {
		extend(number_copy, ZERO_U64);
	}
}

void leftShift(bigInteger *number) {
	uint64_t carry = ZERO_U64;
	for (size_t k = ZERO; k < (*number).size; ++k) {
		const uint64_t MSB = ((*number).content[k] & MOST_SIGNIFICANT_BIT_MASK) ? ONE_U64 : ZERO_U64;
		(*number).content[k] = ((*number).content[k] << ONE) + carry;
		carry = MSB;
	}
}

void rightShift(bigInteger *number) {
	(*number).content[0] = (uint64_t) ((*number).content[0] >> ONE); // divide by 2
	for (size_t i = ONE; i < (*number).size; ++i) {
		if (isOdd((*number).content[i])) {
			(*number).content[i - ONE] = (uint64_t) ((*number).content[i - ONE] | MOST_SIGNIFICANT_BIT_MASK);
		}
		(*number).content[i] = (uint64_t) ((*number).content[i] >> ONE);
	}
}

void parseHexInputAsBigInteger(const char *inputValue, size_t inputSize, bigInteger *outputNumber) {
    size_t currentCell = ZERO;
    size_t currentDigitPositionInBits = ZERO;

    while (inputSize--) {
		const uchar currentDigit = (uchar)inputValue[-1];
		const uchar currentHexDigitAsDecimal = ((currentDigit > (uchar)64u) ? (currentDigit + (uchar)9u) : currentDigit) % (uchar)16u; // based on ASCII table

        // Put current digit in the current cell
		outputNumber->content[currentCell] = (uint64_t)(outputNumber->content[currentCell] | ((uint64_t)currentHexDigitAsDecimal << (uint64_t)currentDigitPositionInBits));
        currentDigitPositionInBits += 4u;
		if (currentDigitPositionInBits == SIXTY_FOUR) {
			currentDigitPositionInBits = ZERO;
			++currentCell;
		}
        inputValue = &inputValue[-1];
	}
}

bigInteger allocateBigInteger(size_t inputSize) {
    bigInteger table;
    table.size = (inputSize + 15u) / 16u;
    table.content = calloc(table.size, sizeof(uint64_t));
    return table;
}
